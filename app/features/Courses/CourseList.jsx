import React from 'react';
import PropTypes from 'prop-types';
import Table from 'antd/lib/table';
import { getNameById, getSelector, getElementId, addExtraAllawa, getStatusById } from '../_shared/services/dataService';
import { Row, Col, Input, Select, Tag, Button, Modal, TimePicker, DatePicker } from 'antd';
import moment from 'moment';

const Search = Input.Search;
const Option = Select.Option;
let type = 'Mid Semester';
const size = 'large';
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;

function handleChange(value) {
	console.log(`selected ${value}`);
	console.log(typeof value);
}

function handleBlur() {
	console.log('blur');
}

function handleFocus() {
	console.log('focus');
}

export default class CourseList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSource: [],
			dataSearch: this.props.dataSource,
			type: 'Mid Semester',
			width: 0,
			height: 0,
			loading: false,
			visible: false,
			visibleSnack: false,
			staff: '',
			start: [],
			end: [],
			date: [],
			extraType: '',
		};

		this.state.dataSource = this.props.dataSource;
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	showModal = () => {
		this.setState({
			visible: true,
		});
	};

	showModalSnack = () => {
		this.setState({
			visibleSnack: true,
		});
	};

	handleOk = () => {
		this.setState({
			loading: false,
			visible: false,
		});
	};

	handleCancel = () => {
		this.setState({
			visible: false,
		});
	};

	handleOkSnack = () => {
		this.setState({
			loading: false,
			visibleSnack: false,
		});
		// const start = this.state.start;
		// const end = this.state.end;
		const p_id = getElementId(this.state.staff, 'personnel')[0].p_id;
		const status = getStatusById(p_id, 'personnel')[0].status;
		const minutes = moment.duration(this.state.end.diff(this.state.start)).asMinutes();
		// console.log('Minutes: ', minutes);

		addExtraAllawa({
			p_id: p_id,
			date: this.state.date.format('LL'),
			duration: minutes,
			extraType: 'hours',
			type: this.state.extraType,
			status: status,
		});
	};

	handleCancelSnack = () => {
		this.setState({
			visibleSnack: false,
		});
	};

	componentWillReceiveProps(nextProps) {
		this.setState({
			dataSource: nextProps.dataSource,
		});
	}

	updateWindowDimensions() {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
		// this.renderDataSource();
	}

	componentWillMount = () => {
		// const dataSource = getSelector('session').map((element, id) => {
		//   return {
		//     ...element,
		//     pid: id + 1,
		//     key: id,
		//     name: getNameById(element.p_id, 'personnel')[0].name
		//   }
		// }).filter(element => element.type === this.state.type);
		// this.setState({
		//   dataSource
		// })
	};

	// componentDidUpdate(prevProps, prevState) {
	// 	const dataSource = getSelector('session')
	// 		.map((element, id) => {
	// 			return {
	// 				...element,
	// 				pid: id + 1,
	// 				key: id,
	// 				name: getNameById(element.p_id, 'personnel')[0].name,
	// 			};
	// 		})
	// 		.filter(element => element.type === this.state.type);
	// 	if (this.state.dataSearch !== dataSource ) {
	// 	  this.setState({ dataSearch: dataSource });
	// 	}
	//   }

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUpdate() {
		// const dataSource = getSelector('session')
		// 	.map((element, id) => {
		// 		return {
		// 			...element,
		// 			pid: id + 1,
		// 			key: id,
		// 			name: getNameById(element.p_id, 'personnel')[0].name,
		// 		};
		// 	})
		// 	.filter(element => element.type === this.state.type);
		// this.setState({ dataSearch: dataSource });
	}

	handleChange = value => {
		this.setState({
			type: value,
		});
	};

	renderSession = period => {
		// console.log('period: ', period);
		if (period === 'session 1') {
			return (
				<div className="action-column grid">
					<Tag color="#87d068">{period}</Tag>
				</div>
			);
		}
		if (period === 'session 2') {
			return (
				<div className="action-column grid">
					<Tag color="#2db7f5">{period}</Tag>
				</div>
			);
		}
		if (period === 'session 3') {
			return (
				<div className="action-column grid">
					<Tag color="#00bcd4">{period}</Tag>
				</div>
			);
		}
		if (period === 'session 4') {
			return (
				<div className="action-column grid">
					<Tag color="#87d068">{period}</Tag>
				</div>
			);
		}
		if (period === 'session 5') {
			return (
				<div className="action-column grid">
					<Tag color="#2db7f5">{period}</Tag>
				</div>
			);
		}
		if (period === 'session 6') {
			return (
				<div className="action-column grid">
					<Tag color="#00bcd4">{period}</Tag>
				</div>
			);
		}
	};

	renderStaffData = () => {
		const staff = getSelector('personnel').map((element, index) => {
			// console.log(element.name);
			return (
				<Option value={element.name} key={element.name + index}>
					{element.name}
				</Option>
			);
		});

		return staff;
	};

	onSearch = e => {
		// console.log(e.target.value);
		const value = e.target.value.toLowerCase();
		const newData = this.state.dataSource.filter(s => s.name.toLowerCase().search(value) !== -1);
		// console.log('newData: ', dataSource);
		this.setState({ dataSearch: newData });
	};

	renderDataSource = () => {
		const dataSource = getSelector('session')
			.map((element, id) => {
				return {
					...element,
					pid: id + 1,
					key: id,
					name: getNameById(element.p_id, 'personnel')[0].name,
				};
			})
			.filter(element => element.type === this.state.type);
		this.setState({ dataSearch: dataSource });
		// console.log(this.state.dataSearch);
	};

	render() {
		const { visible, loading, visibleSnack, dataSearch } = this.state;
		const dataSource = getSelector('session')
			.map((element, id) => {
				return {
					...element,
					pid: id + 1,
					key: id,
					name: getNameById(element.p_id, 'personnel')[0].name,
				};
			})
			.filter(element => element.type === this.state.type);

		const columns: Array<Object> = [
			{ title: 'ID', dataIndex: 'pid', key: 'pid' },
			{ title: 'Name', dataIndex: 'name', key: 'name' },
			{ title: 'Date', dataIndex: 'date', key: 'date' },
			{ title: 'Start', dataIndex: 'start', key: 'start' },
			{ title: 'End', dataIndex: 'end', key: 'end' },
			{ title: 'Minutes', dataIndex: 'duration_mins', key: 'duration_mins' },
			{
				title: 'Session',
				render: (text, record) => this.renderSession(text.period),
			},
			{
				title: ' ',
				render: (text, record) => (
					<div className="action-column grid">
						{
							// 	<button
							// 	className="edit column"
							// 	onClick={() => {
							// 		// console.log(record);
							// 		this.props.onEditClicked(record);
							// 	}}
							// >
							// 	Edit
							// </button>
						}

						<button className="delete column" onClick={() => this.props.onDeleteClicked(record)}>
							Delete
						</button>
					</div>
				),
			},
		];

		return (
			<div className="course-list column">
				{/* <h2>List of Courses</h2>
            <div className="table-container">
                <Table
                    className="course-list-table"
                    dataSource={dataSource}
                    columns={columns} />
            </div> */}
				<div className="list-container">
					<div>
						<Row className="filter-options-course">
							<Col span={8}>
								<div className="filter-container-elements">
									<Select
										defaultValue="Mid Semester"
										style={{ width: '100%' }}
										onChange={e => this.handleChange(e)}
										size="large"
									>
										<Option value="Mid Semester">Mid Semester</Option>
										<Option value="End of Semester">End of Semester</Option>
									</Select>
								</div>
							</Col>
							<Col span={8}>
								<div className="filter-container-elements">
									<Button size="large" style={{ width: '80%' }} onClick={this.showModalSnack}>
										Add hours
									</Button>
									<Modal
										visible={visibleSnack}
										title="Add hours"
										onOk={this.handleOkSnack}
										onCancel={this.handleCancelSnack}
									>
										<div style={{ width: '80%', margin: '1rem auto', display: 'block' }}>
											<Row gutter={16} style={{ padding: '1rem auto' }}>
												<Col span={12}>
													<Select
														// className="form-input"
														size={size}
														onChange={e => {
															this.setState({ extraType: e });
														}}
														placeholder="e.g. Mid semester"
														style={{ width: '100%' }}
													>
														<Option value="Mid Semester">Mid Semester</Option>
														<Option value="End of Semester">End of Semester</Option>
													</Select>
												</Col>
												<Col span={12}>
													<DatePicker
														style={{ width: '100%' }}
														size={size}
														onChange={e => {
															this.setState({ date: e });
														}}
													/>
												</Col>
											</Row>
											<Row>
												<Select
													showSearch
													size={size}
													style={{ width: '100%', margin: '1rem auto', display: 'block' }}
													placeholder="Select a person"
													optionFilterProp="children"
													onChange={e => {
														this.setState({ staff: e });
													}}
													onFocus={handleFocus}
													onBlur={handleBlur}
													filterOption={(input, option) =>
														option.props.children
															.toLowerCase()
															.indexOf(input.toLowerCase()) >= 0
													}
												>
													{this.renderStaffData()}
												</Select>
											</Row>

											<Row gutter={16} style={{ padding: '1rem auto' }}>
												<Col span={12}>
													<TimePicker
														onChange={e => {
															this.setState({ start: e });
														}}
														size={size}
														style={{ width: '100%' }}
														defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
													/>
												</Col>
												<Col span={12}>
													<TimePicker
														onChange={e => {
															this.setState({ end: e });
														}}
														size={size}
														style={{ width: '100%' }}
														defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
													/>
												</Col>
											</Row>
										</div>
									</Modal>
								</div>
							</Col>
							<Col span={8}>
								<div className="course-search-container">
									<Search
										placeholder="input search text"
										size="large"
										onChange={this.onSearch}
										// style={{ width: 250 }}
									/>
								</div>
							</Col>
						</Row>
					</div>
					<h2>List of Allocations</h2>
					<div className="table-container">
						<Table
							className="course-list-table"
							// loading
							// style={{ height: "10rem"}}
							pagination={{ pageSize: this.state.height / 100 }}
							// size="middle"
							dataSource={this.state.dataSearch.length == 0 ? this.state.dataSearch : dataSource}
							columns={columns}
						/>
					</div>
				</div>
			</div>
		);
	}
}

CourseList.propTypes = {
	onEditClicked: PropTypes.func.isRequired,
	onDeleteClicked: PropTypes.func.isRequired,

	dataSource: PropTypes.arrayOf(PropTypes.shape()),
};

CourseList.defaultProps = {
	dataSource: [],
};
