import React, { Component } from 'react'
import './settings.css'
import { Row, Col, Card, Button, Modal, notification } from 'antd'
import { resetTable } from '../_shared/services/dataService'
import Classrooom from '../_shared/assets/classroom.png'
import Cash from '../_shared/assets/money-bag.png'
import Exam from '../_shared/assets/exam.png'
import ExamSession from '../_shared/assets/stopwatch.png'
import MidSession from '../_shared/assets/wall-calendar.png'
import Allocation from '../_shared/assets/collaboration (1).png'
import Snack from '../_shared/assets/fast-food.png'
import All from '../_shared/assets/recycle.png'
import swal from 'sweetalert'

const tables = ['invigilation_allowances', 'snack_allowances', 'session', 'personnel']
const tables1 = ['invigilation_allowances', 'snack_allowances', 'session' ]
export default class DataReset extends Component {
  openNotificationWithIcon = table => {
    notification.error({
      message: 'Oops! There was an error deleting',
      description: `${table} can not be deleted because it has default values used for computation. Please reconsider updating. Thank you.`
    })
  }

  handleOk = params => {
    resetTable(params)
    swal('Good job!', 'Data deleted successfully!', 'success')
  }

  handleCancel = e => {
    console.log(e)
    // this.setState({
    //   visible: false
    // })
  }

  warning = table => {
    // return confirm({
    //   title: 'This is a warning message',
    //   content: 'All data in this table will be lost when you proceed',
    //   okText: 'Proceed',
    //   cancelText: 'Cancel',
    //   onOk: () => handleOk(table),
    //   onCancel() {}
    // })
    swal({
      title: 'Are you sure?',
      text: `Once deleted, you will not be able to recover ${table} data`,
      icon: 'warning',
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if (willDelete) {
        resetTable(table)
        swal(`Poof! Your ${table} data file has been deleted!`, {
          icon: 'success'
        })
      } else {
        swal(`Your ${table} data is safe!`)
      }
    })
  }

  render () {
    return (
      <div className='class-container'>
        <div className='class-container-element' style={{ display: 'flex' }}>
          <div className='content-container'>
            <Row>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={Classrooom} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Staff data</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.warning(['personnel'])}>
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={Allocation} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Staff allocations</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.warning(tables1)}>
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={MidSession} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Mid sem sessions</p>
                  <Button
                    type='danger'
                    style={{ width: '100%' }}
                    onClick={() => this.openNotificationWithIcon('Mid sem sessions')}
                  >
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={ExamSession} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>End of sem sessions</p>
                  <Button
                    type='danger'
                    style={{ width: '100%' }}
                    onClick={() => this.openNotificationWithIcon('End of sem sessions')}
                  >
                    Reset
                  </Button>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={Cash} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Cash config</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.openNotificationWithIcon('Cash config')}>
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={Snack} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Snack Report</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.warning(tables1)}>
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={Exam} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>Invigilation Report</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.warning(tables1)}>
                    Reset
                  </Button>
                </Card>
              </Col>
              <Col span={6} style={{ padding: '1rem' }}>
                <Card style={{ height: '15rem' }}>
                  <img src={All} alt='classroom' className='img-container' />
                  <p style={{ textAlign: 'center', paddingBottom: '0.5rem' }}>All data</p>
                  <Button type='danger' style={{ width: '100%' }} onClick={() => this.warning(tables)}>
                    Reset All
                  </Button>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}
