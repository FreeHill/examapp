import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Tabs, Modal, Icon, Collapse } from 'antd'
import {
  addCashItem,
  updateCashItem,
  deleteCashItem
} from '../../_shared/services/dataService'
import Newmidsem from './NewMidsem'
import * as Actions from './collegeActions'
import classImage from '../../_shared/assets/classroom.png'
import MidrateList from './midrateList'
import '../settings.css'
import swal from 'sweetalert';

const confirm = Modal.confirm
const DEFAULT_EDIT_CONFIG = {
  // Edit configuration
  editMode: false, // Flag to indicate whether we are editting or adding
  editID: -1
}

class MidrateSettings extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      ...DEFAULT_EDIT_CONFIG,
      field: {},
      visible: false
    }

    this.cancelEditMode = this.cancelEditMode.bind(this)
    this.collegeEditted = this.collegeEditted.bind(this)
    this.collegeRemoved = this.collegeRemoved.bind(this)
    this.triggerEditMode = this.triggerEditMode.bind(this)
  }

  componentWillMount () {
    document.title = 'Mid Semester Cash Management'

    // Check if the redux store is empty. If so, check in the database for data
  }

  // Return from the Edit mode to the Add mode
  cancelEditMode () {
    this.setState({
      ...DEFAULT_EDIT_CONFIG,
      editMode: false
    })
  }

  // Edit (or add) a college
  collegeEditted (college: Object) {
    if (this.state.editMode === true && college.id !== -1) {
      // console.log(college);
      // We are in edit mode. Find the college and splice the list
      // const doEditing = (array: Array<Object>) => {
      //     return array.map(element => (element.id === college.id) ? college : element);
      // };

      this.props.collegeSecondEditted(college)
      // console.log(college);
      updateCashItem(college)
      swal("Good job!", "Record was updated succesfully", "success");
      this.cancelEditMode()
    } else {
      this.props.collegeAdded(college)
      addCashItem(college)
      swal("Good job!", "Record was added succesfully", "success");
    }

    this.cancelEditMode()
  }

  handleOk = () => {
    this.setState({
      visible: false
    })
  }

  handleCancel = () => {
    this.setState({
      visible: false
    })
  }

  collegeRemoved (college: Object) {
    return confirm({
      title: `Do you want to delete ${college.iten} item?`,
      content: `When the Yes button is clicked, ${college.iten} item will be 
                deleted permanently with all data related to it.
                Please proceed with caution`,
      okText: 'Yes',
      visible: this.state.visible,
      okType: 'danger',
      cancelText: 'No',
      onOk: () => {
        this.props.collegeRemoved(college)
        this.handleOk()
        deleteCashItem(college)
      },
      onCancel: () => this.handleCancel()
    })
  }

  triggerEditMode (college: Object) {
    this.setState({
      editID: college.id,
      editMode: true,
      field: college
    })
  }

  renderCashItems () {
    return (
      <div style={{ height: '100%', width: '100%' }}>
        <Row className='college-Row'>
          <Col span={14} style={{ height: '100%' }}>
            <Newmidsem
              editMode={this.state.editMode}
              id={this.state.editID}
              fieldData={this.state.field}
              oncollegeEditted={this.collegeEditted}
              onCancel={this.cancelEditMode}
            />
          </Col>
          <div className='college-content-separater' />
          <Col span={10}>
            <MidrateList
              onEditClicked={this.triggerEditMode}
              onDeleteClicked={this.collegeRemoved}
              dataSource={this.props.colleges}
            />
          </Col>
        </Row>
      </div>
    )
  }

  render () {
    return (
      <div className='class-container'>
        <div className='class-container-element' style={{ margin: "2rem auto"}}>{this.renderCashItems()}</div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    colleges: state.colleges
  }
}

export default connect(
  mapStateToProps,
  { ...Actions }
)(MidrateSettings);
