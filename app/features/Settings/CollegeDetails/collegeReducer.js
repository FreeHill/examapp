import initialStoreState from '../../../store/initialStoreState';
import { ActionTypes } from '../../../constants';

const initialState = initialStoreState.colleges;
const types = ActionTypes.colleges;

export default function (state = initialState, action) {
    let newstate = state;
    switch (action.type) {
        case types.COLLEGES_ADDED:
            return action.payload;

        case types.COLLEGE_ADDED:
            newstate = state;
            newstate.push({
                ...action.payload,
                id: state.length
            });
            return newstate;
            
        case types.COLLEGE_REMOVED:
            newstate = state;
            // console.log("New state: ", newstate, "Action: ", action.payload);
            newstate = state.filter((elem: Object) => (elem.item !== action.payload.item && elem.type !== action.payload.type));
            return newstate;

        case types.COLLEGE_EDITTED:
            newstate = state;
            // console.log("New state: ", newstate, "Action: ", action.payload);
            newstate = newstate.map(element => ((element.type !== action.payload.type) ? action.payload : element));
            return newstate; 

        case types.COLLEGE2_EDITTED:
            newstate = state;
           // console.log("New state: ", newstate, "Action: ", action.payload);
            newstate = newstate.map(element => ((element.type !== action.payload.type) ? action.payload : element));
            return newstate; 
                 
        default:
            return state;
    }
}
