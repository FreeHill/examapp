import CollegeSettings from './CollegeDetails/College';
import Classes from '../Classes';
import Courses from '../Courses';
import Settings from '../Settings';
import Teachers from '../Teachers';
import DataReset from './dataReset';

export default {
    CollegeSettings,
    Classes,
    Courses,
    Settings,
    Teachers,
    DataReset
};
