import { ipcRenderer } from 'electron'
import React, { Component } from 'react'
import swal from 'sweetalert'
import { Table, Button, Row, Col, Icon, Select } from 'antd'
import { generateInvigilation } from '../_shared/services/dataService'
import { InvigilationReport } from './invigilationReport'
import '../Teachers/teachers'
// import {ViewPDF} from '../_shared/services/pdfGenerator';

const columns: Array<Object> = [
  { title: 'SN', dataIndex: 'pid', key: 'id' },
  // { title: 'Title', dataIndex: 'title', key: 'title' },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Time/mins', dataIndex: 'duration_total', key: 'duration_total' },
  { title: 'Status', dataIndex: 'status', key: 'status' },
  { title: 'Rate/Hr', dataIndex: 'rate_hr', key: 'rate_hr' },
  { title: 'Amt(GHC)', dataIndex: 'amount', key: 'amount' },
  { title: 'Tax', dataIndex: 'tax', key: 'tax' },
  { title: 'Amt Due (GHC)', dataIndex: 'amount_due', key: 'amount_due' }
]

const GENERATION_SUCCESS_CHANNEL_NAME = 'GENERATION_SUCCESS'
const GENERATION_CHANNEL_NAME = 'GENERATION'
const Option = Select.Option
var app = require('electron').remote
var dialog = app.dialog
var fs = require('fs')

function arrayToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

  return array.reduce((str, next) => {
      str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
      return str;
     }, str);
}
class Invigilation extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dataSource: [],
      sem: '',
      year: '',
      type: 'Mid Semester',
      width: 0,
      height: 0
    }

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount () {
    ipcRenderer.on(GENERATION_SUCCESS_CHANNEL_NAME, () => {
      console.log('Rendering complete')
    })
  }

  handleChange = (value) => {
    this.setState({
      type: value
    })
  }

  componentWillMount () {
    
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  render () {
    const dataSource = generateInvigilation().map((element, id) => {
      return {
        ...element,
        pid: id + 1,
        key: id
      }
    }).filter(element => element.type === this.state.type);

    const handleGeneration = () => {
      let termDetails = {
        year: this.state.year,
        sem: this.state.sem
      }

      var content = arrayToCSV(dataSource);
      dialog.showSaveDialog(fileName => {
        if (fileName === undefined) {
          swal ( "Oops" ,  "Something went wrong! we couldn't create your file" ,  "error" )
          return
        }

        // fileName is a string that contains the path and filename created in the save file dialog.
        fs.writeFile(fileName, content, err => {
          if (err) {
            swal ( "Oops" ,  "Something went wrong! we couldn't write your file" ,  "error" )
          }

          swal ( "Good job!" ,  "File created successfully!" ,  "success" )
        })
      })
      // ipcRenderer.send(
      //   GENERATION_CHANNEL_NAME,
      //   InvigilationReport(dataSource, termDetails)
      // )
    }

    return (
      <div className='table-container'>
        <Row style={{ marginBottom: '1rem' }}>
          <Col span={6} style={{ float: 'left' }}>
            <div>
              {/* <p style={{ margin: '0rem 5rem' }} className='report-termdetails'>
                <Icon type='edit' style={{ paddingRight: '0.5rem' }} />
                Exam type
              </p> */}
              <Select
                defaultValue='Mid Semester'
                style={{ margin: '0rem 1rem', width: '80%' }}
                onChange={e => this.handleChange(e)}
                size='large'
              >
                <Option value='Mid Semester'>Mid Semester</Option>
                <Option value='End of Semester'>End of Semester</Option>
              </Select>
            </div>
          </Col>
          <Col span={6} style={{ textAlign: 'center' }}>
            <div/>
          </Col>
          <Col span={6} style={{ textAlign: 'center' }}>
            <div/>
          </Col>
          <Col span={6}>
            <div>
              <Button
                type='primary'
                size='large'
                className="generate-button"
                onClick={() => handleGeneration()}
                style={{ float: 'right' }}
                // disabled={!this.state.year && !this.state.year}
              >
                Generate Report
              </Button>
            </div>
          </Col>
        </Row>
        <Table
          className='snack-list-table'
          dataSource={dataSource}
          pagination={{ pageSize: (this.state.height)/70 }}
          style={{ margin: '0rem 1rem', border: '1px solid #d4d4d5' }}
          columns={columns}
        />
      </div>
    )
  }

  // componentWillUnmount () {
  //   ipcRenderer.removeListener(GENERATION_SUCCESS_CHANNEL_NAME)
  // }
}

export default Invigilation
