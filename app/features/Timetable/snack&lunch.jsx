import React, { Component } from 'react'
import { ipcRenderer } from 'electron'
import { SnackReport } from './snackReport'
import swal from 'sweetalert';
import { SnackCalculation } from '../_shared/services/dataService'
import { Table, Button, Select, Row, Col, Icon } from 'antd'
import './timetable.css'

const Option = Select.Option
var app = require('electron').remote
var dialog = app.dialog
var fs = require('fs') // Load the File System to execute our common tasks (CRUD)

function handleChange (value) {
  console.log(`selected ${value}`)
}

function handleBlur () {
  console.log('blur')
}

function handleFocus () {
  console.log('focus')
}

function arrayToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

  return array.reduce((str, next) => {
      str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
      return str;
     }, str);
}

const columns: Array<Object> = [
  { title: 'SN', dataIndex: 'pid', key: 'id' },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Sessions', dataIndex: 'sessions', key: 'sessions' },
  { title: 'Amount', dataIndex: 'amount', key: 'amount' }
]

const GENERATION_SUCCESS_CHANNEL_NAME = 'GENERATION_SUCCESS'
const GENERATION_CHANNEL_NAME = 'GENERATION'

class Snack extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dataSource: [],
      sem: '',
      year: '',
      type: 'Mid Semester',
      width: 0,
      height: 0
    }

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
  }

  componentWillUpate () {
    const dataSource = SnackCalculation()
      .map((element, id) => {
        return {
          ...element,
          pid: id + 1,
          key: id
        }
      })
      .filter(element => element.type === this.state.type)

    this.setState({
      dataSource
    })

    // console.log(this.state.dataSource)
  }

  updateWindowDimensions () {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  componentDidMount () {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  handleChange = value => {
    this.setState({
      type: value
    })
  }

  render () {
    const dataSource = SnackCalculation()
      .map((element, id) => {
        return {
          ...element,
          pid: id + 1,
          key: id
        }
      })
      .filter(element => element.type === this.state.type)

    const handleGeneration = () => {
      let termDetails = {
        year: this.state.year,
        sem: this.state.sem
      }

      // return new Promise((resolve, reject) => {

      //   ipcRenderer.send(
      //     GENERATION_CHANNEL_NAME,
      //     SnackReport(dataSource, termDetails))

      // })
      // console.log(dataSource);
      // let content = new ObjectsToCsv(dataSource);
      // console.log(content.data);
      var content = arrayToCSV(dataSource);

      dialog.showSaveDialog(fileName => {
        if (fileName === undefined) {
          swal ( "Oops" ,  "Something went wrong! we couldn't create your file" ,  "error" )
          return
        }

        // fileName is a string that contains the path and filename created in the save file dialog.
        fs.writeFile(fileName, content, err => {
          if (err) {
            swal ( "Oops" ,  "Something went wrong! we couldn't write your file" ,  "error" )
          }

          swal ( "Good job!" ,  "File created successfully!" ,  "success" )
        })
      })
    }
    return (
      <div className='table-container'>
        <Row style={{ marginBottom: '1rem' }}>
          <Col span={6} style={{ float: 'left' }}>
            <div>
              {/* <p style={{ margin: '0rem 5rem' }} className='report-termdetails'>
                <Icon type='edit' style={{ paddingRight: '0.5rem' }} />
                Exam type
              </p> */}
              <Select
                defaultValue='Mid Semester'
                style={{ margin: '0rem 1rem', width: '80%' }}
                onChange={e => this.handleChange(e)}
                size='large'
              >
                <Option value='Mid Semester'>Mid Semester</Option>
                <Option value='End of Semester'>End of Semester</Option>
              </Select>
            </div>
          </Col>
          <Col span={6} style={{ textAlign: 'center' }}>
            <div />
          </Col>
          <Col span={6} style={{ textAlign: 'center' }}>
            <div/>
          </Col>
          <Col span={6}>
            <div>
              <Button
                type='primary'
                size='large'
                className='generate-button'
                onClick={() => handleGeneration()}
                style={{ float: 'right' }}
                // disabled={!this.state.year && !this.state.year}
              >
                Generate Report
              </Button>
            </div>
          </Col>
        </Row>
        <div className='table-generater-container'>
          <Table
            className='snack-list-table'
            dataSource={dataSource}
            pagination={{ pageSize: this.state.height / 70 }}
            style={{ margin: '0rem 1rem', border: '1px solid #d4d4d5' }}
            columns={columns}
          />
        </div>
      </div>
    )
  }
}

export default Snack
