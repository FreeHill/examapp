export const SQLiteSchema = [
  {
    name: 'cash_item',
    createQuery: `
            CREATE TABLE IF NOT EXISTS  cash_item (
                item_id integer NOT NULL,
                item text,
                item_amount integer,
                type text,
                PRIMARY KEY (item_id)
            )`
  },
  {
    name: 'invigilation_allowances',
    createQuery: `
            CREATE TABLE IF NOT EXISTS  invigilation_allowances (
                ia_id integer NOT NULL,
                date text,
                session_count integer,
                duration_total integer,
                p_id integer,
                rate_min integer,
                rate_hr integer,
                type text,
                PRIMARY KEY (ia_id)
              )`
  },
  {
    name: 'personnel',
    createQuery: `
            CREATE TABLE IF NOT EXISTS  personnel (
                p_id integer NOT NULL,
                name text,
                member text,
                status text,
                PRIMARY KEY (p_id)
            )`
  },
  {
    name: 's_config',
    createQuery: ` 
            CREATE TABLE IF NOT EXISTS  s_config (
                s_config_id integer NOT NULL,
                snack_count integer,
                amount integer,
                session_count integer,
                type text,
                PRIMARY KEY (s_config_id)
              )`
  },
  {
    name: 'session',
    createQuery: ` 
            CREATE TABLE IF NOT EXISTS  session (
                session_id integer NOT NULL,
                period text,
                start text,
                end text,
                date text,
                p_id intger,
                duration_mins intger,
                type text,
                PRIMARY KEY (session_id)
              )`
  },
  {
    name: 'snack_allowances',
    createQuery: `
            CREATE TABLE IF NOT EXISTS  snack_allowances (
                sa_id integer NOT NULL,
                s_config_id integer,
                p_id integer,
                date text,
                type text,
                PRIMARY KEY (sa_id)
            )`
  },
  {
    name: 'staff',
    createQuery: `
            CREATE TABLE IF NOT EXISTS  staff (
                s_id integer NOT NULL,
                status text,
                p_id integer,
                PRIMARY KEY (s_id)
            )`
  },
  {
    name: 'init',
    createQuery: `
            CREATE TABLE IF NOT EXISTS init(
                id integer AUTO_INCREMENT PRIMARY KEY,
                status integer NOT NULL
            )
        `
  }
]

export const cash_items = [
  { name: 'snack', amount: 10, type: 'End of Semester' },
  { name: 'tax', amount: 0.1, type: 'End of Semester' },
  { name: 'rate_senior', amount: 20, type: 'End of Semester' },
  { name: 'rate_non_senior', amount: 10, type: 'End of Semester' },
  { name: 'snack', amount: 5, type: 'Mid Semester' },
  { name: 'tax', amount: 0.1, type: 'Mid Semester' },
  { name: 'rate_senior', amount: 20, type: 'Mid Semester' },
  { name: 'rate_non_senior', amount: 10, type: 'Mid Semester' }
]

export const configs = [
  { snack: 1, amount: 10, session: 1, type: 'End of Semester' },
  { snack: 2, amount: 20, session: 2, type: 'End of Semester' },
  { snack: 3, amount: 30, session: 3, type: 'End of Semester' },
  { snack: 1, amount: 5, session: 1, type: 'Mid Semester' },
  { snack: 2, amount: 10, session: 2, type: 'Mid Semester' },
  { snack: 3, amount: 15, session: 3, type: 'Mid Semester' },
  { snack: 4, amount: 20, session: 4, type: 'Mid Semester' },
  { snack: 5, amount: 25, session: 5, type: 'Mid Semester' },
  { snack: 6, amount: 30, session: 6, type: 'Mid Semester' }
]
