import React from 'react';
import PropTypes from 'prop-types';
import { Form, Icon, Input, Button, Select, InputNumber, Row, Col } from 'antd';
import { getSelector } from '../_shared/services/dataService';
import swal from 'sweetalert';


const FormItem = Form.Item;
const Option = Select.Option;

class NewMidsemForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            counter: 1
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
		// To disabled submit button at the beginning and checks validations.
        this.props.form.validateFields();
    }
    
    componentWillReceiveProps(nextProps) {
        if (
            Object.keys(nextProps.fieldData).length !== 0 &&
            nextProps.editMode === true &&
            this.state.counter > 0
          ) {
            this.props.form.setFieldsValue({
                session: nextProps.fieldData.session_count,
                snack_count: nextProps.fieldData.snack_count,
                amount: nextProps.fieldData.amount
            });
            this.setState({ counter: -1 });
          }
    }

    // reset form data when submitted
    handleReset = () => {
        this.props.form.resetFields();
        this.setState({ counter: 1 });   
    }

    indexError = () => {
        swal("Oops!", "We encontered an error trying to add your item", "error");
    }

    // function called when the button is selected    
    handleSubmit = (e) => {
        e.preventDefault();
		this.props.form.validateFields((err, values) => {
            const id = (this.props.editMode) ? this.props.id : -1;

            let hasError: boolean = false;
            this.props.midsem.forEach((mid: Object) => {
                if (
                mid.session_count === values.session &&
                this.props.editMode === false && mid.type === "Mid Semester"
                ) {
                hasError = true;
                }
            });

            if (hasError) {
                this.indexError();
                return;
            }

			if (!err) {
                const midsemDetail: Object = {
                    id: this.props.id,
                    session_count: values.session,
                    snack_count: values.snack_count,
                    amount: values.amount,
                    type: 'Mid Semester'
                };
                // console.log(midsemDetail);
                this.props.onMidsemEditted(midsemDetail);
            }

            this.handleReset();
		});
    }


    // this checks the form validation  
    hasErrors(fieldsError) {
        const fromAnt = (Object.keys(fieldsError).some(field => fieldsError[field]));
        return fromAnt;
    }

    renderCancel() {
        return this.props.editMode === false ? null : (
          <button
            type="button"
            style={{ margin: '0px auto', width: '100%' }}
            onClick={() => {
              this.props.onCancel();
              this.handleReset();
            }}
          >
            Cancel
          </button>
        );
    }

    render() {
        const header: string = (this.props.editMode) ? 'Edit' : 'New';
        const buttonText: string = (this.props.editMode) ? 'Edit' : 'Add';

        const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched, getFieldValue } = this.props.form;
		const classNameError = isFieldTouched('session') && getFieldError('session');
        const classSizeError = isFieldTouched('amount') && getFieldError('amount'); 
        const snackCountError = isFieldTouched('snack_count') && getFieldError('snack_count');
        // const othersError = getFieldError('otherSize');   

        const className = getFieldValue('session');
        const classSize = getFieldValue('amount');
        const snackcountSize = getFieldValue('snack_count');

        const isEmpty = !className || !classSize || snackcountSize;

        return (
            <div>
                <Form onSubmit={this.handleSubmit} className="column new-midsem">
                <h2>{header} configuration</h2>
                    <label htmlFor="new-midsem-name">Session</label>
                    <FormItem 
                        style={{textAlign: '-webkit-center'}}
                        hasFeedback  
                        // label="Username"
                        validateStatus={classNameError ? 'error' : ''}
                        help={classNameError || ''}
                    >
                        {getFieldDecorator('session', {
                            rules: [{ required: true,  type: 'number', message: 'enter session!' }],
                        })(
                            <InputNumber min={1} max={20} style={{ width: '100%' }} placeholder="e.g. " />
                        )}
                    </FormItem>
                    <label htmlFor="new-midsem-name">Snack</label>
                    <FormItem 
                        style={{textAlign: '-webkit-center'}}
                        hasFeedback  
                        // label="Username"
                        validateStatus={snackCountError ? 'error' : ''}
                        help={snackCountError || ''}
                    >
                        {getFieldDecorator('snack_count', {
                            rules: [{ required: true,  type: 'number', message: 'enter session!' }],
                        })(
                            <InputNumber min={1} max={1000} style={{ width: '100%' }} placeholder="e.g. " />
                        )}
                    </FormItem>
                        <label htmlFor="new-midsem-std-cap">Amount</label>
                            <FormItem 
                                // style={{textAlign: '-webkit-center'}}
                                hasFeedback
                                validateStatus={classSizeError ? 'error' : ''}
                                help={classSizeError || ''}
                            >
                                {getFieldDecorator('amount', {
                                    rules: [{
                                        required: true, type: 'number', message: 'snack amount!',
                                    }],
                                })(
                                    <InputNumber min={1} max={1000} style={{ width: '100%', marginRight:'0.5rem' }} placeholder="e.g. 50" />
                                )}
                            </FormItem>   
                    <FormItem>
                        <Button 
                            type="primary" 
                            size={'large'} 
                            // className="add-mid-button" 
                            style={{ margin: '20px auto', width: '100%'}}
                            htmlType="submit" 
                            disabled={this.hasErrors(getFieldsError())}>{buttonText} Configuration</Button>
                    </FormItem>
                    {this.renderCancel()}
                </Form>
            </div> 
        );
    }
}

export const NewMidsem = Form.create()(NewMidsemForm);

NewMidsem.defaultProps = {
    id: -1,
    name: '',
    classSize: '',
    otherSize: ''
};

NewMidsem.propTypes = {
    // Edit configuration
    editMode: PropTypes.bool.isRequired,
    id: PropTypes.number,
    name: PropTypes.string,
    classSize: PropTypes.string,
    otherSize: PropTypes.string,

    onMidsemEditted: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired
};